import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter as Router, Switch, Route, Link } from 'react-router-dom';
import './App.css';
import './bs/css/bootstrap.min.css';
import { Provider } from 'react-redux';
import store from './store';

import jwt_decode from 'jwt-decode';
import setAuthToken from './setAuthToken';
import { setCurrentUser, logoutUser } from './actions/authentication';

import Navbar from './components/Navbar';
import Create from './components/create.component';
import Edit from './components/edit.component';
import Index from './components/index.component';
import Register from './components/Register';
import Login from './components/Login';


if(localStorage.jwtToken) {
  setAuthToken(localStorage.jwtToken);
  const decoded = jwt_decode(localStorage.jwtToken);
  store.dispatch(setCurrentUser(decoded));

  const currentTime = Date.now() / 1000;
  if(decoded.exp < currentTime) {
    store.dispatch(logoutUser());
    window.location.href = '/login'
  }
}

class App extends Component {
  render() {
    return (
    <Provider store = { store }>
      <Router>
      <div className="container">
      <Navbar />
         <br/>
        <h2>Welcome to React CRUD Tutorial</h2> <br/>
        <Switch>
            <Route exact path='/create' component={ Create } />
            <Route path='/edit/:id' component={ Edit } />
            <Route path='/index' component={ Index } />
            <Route exact path="/register" component={ Register } />
            <Route exact path="/login" component={ Login } />
        </Switch>
      </div>
    </Router>
    </Provider>
    );
  }
}

export default App;
