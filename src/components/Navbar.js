// Navbar.js

import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { logoutUser } from '../actions/authentication';
import { withRouter } from 'react-router-dom';

class Navbar extends Component {

      onLogout(e) {
         e.preventDefault();
         this.props.logoutUser(this.props.history);
     }

    render() {

      const {isAuthenticated, user} = this.props.auth;
      const authLinks = (
          <ul className="navbar-nav ml-auto">
          <li class="nav-item">
              <a class="nav-link" href="#">Home</a>
          </li>
          <li className="nav-item">
            <Link to={'/create'} className="nav-link">Create</Link>
          </li>
          <li className="nav-item">
            <Link to={'/index'} className="nav-link">Index</Link>
          </li>
              <a href="#" className="nav-link" onClick={this.onLogout.bind(this)}>
                  <img src={user.avatar} alt={user.name} title={user.name}
                      className="rounded-circle"
                      style={{ width: '25px', marginRight: '5px'}} />
                          Logout
                        </a>
                    </ul>
          )
          const guestLinks = (
          <ul className="navbar-nav ml-auto">
              <li className="nav-item">
                  <Link className="nav-link" to="/register">Sign Up</Link>
              </li>
              <li className="nav-item">
                  <Link className="nav-link" to="/login">Sign In</Link>
              </li>
          </ul>
          )
        // return(
        //     <nav class="navbar navbar-expand-lg navbar-light bg-light">
        //         <a class="navbar-brand" href="#">Redux Auth</a>
        //         <div class="collapse navbar-collapse" id="navbarSupportedContent">
        //             <ul class="navbar-nav ml-auto">
        //                 <li class="nav-item">
        //                     <a class="nav-link" href="#">Home</a>
        //                 </li>
        //                 <li className="nav-item">
        //                   <Link to={'/create'} className="nav-link">Create</Link>
        //                 </li>
        //                 <li className="nav-item">
        //                   <Link to={'/index'} className="nav-link">Index</Link>
        //                 </li>
        //                 <li class="nav-item">
        //                     <a class="nav-link" href="#">Register</a>
        //                 </li>
        //                 <li class="nav-item">
        //                     <a class="nav-link" href="#">Login</a>
        //                 </li>
        //             </ul>
        //         </div>
        //     </nav>
        // )

      return(
      <nav className="navbar navbar-expand-lg navbar-light bg-light">
          <Link className="navbar-brand" to="/">Redux Node Auth</Link>
          <div className="collapse navbar-collapse" id="navbarSupportedContent">
              {isAuthenticated ? authLinks : guestLinks}
          </div>
      </nav>
      )
    }
}

Navbar.propTypes = {
    logoutUser: PropTypes.func.isRequired,
    auth: PropTypes.object.isRequired
}

const mapStateToProps = (state) => ({
    auth: state.auth
})

export default connect(mapStateToProps, { logoutUser })(withRouter(Navbar));
